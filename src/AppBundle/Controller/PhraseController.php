<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Phrase;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/{_locale}", requirements = {"_locale" : "en|ru"})
 */
class PhraseController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request, EntityManagerInterface $em)
    {
        $locale = $this->get('session')->get('_locale');
        $phrases = $this->getDoctrine()->getRepository(Phrase::class)->findAll();
        $form = $this->createFormBuilder()
            ->setAction("/{$locale}/")
            ->add('phrase',
                TextType::class,
                ['label' => $this->get('translator')->trans('phrases.phrase.phrase', [], 'message')])
            ->add('save',
                SubmitType::class,
                ['label' => $this->get('translator')->trans('phrases.phrase.add', [], 'message')])
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            $phrase = new Phrase();
            $phrase
                ->setDate(new \DateTime())
                ->translate('ru')
                ->setPhrase($data['phrase']);
            $em->persist($phrase);
            $phrase->mergeNewTranslations();
            $em->flush();
            return $this->redirectToRoute('homepage');
        }

        return $this->render('@App/index.html.twig', [
            'phrases' => $phrases,
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/translate/{id}", requirements={"id" : "\d"}, name="translate")
     */
    public function translatePageAction($id, Request $request)
    {
        if (!$this->getUser()){
            return $this->redirect('/login');
        }
        $post = Request::createFromGlobals();

        if ($post->request->has('submit')) {
            $name = $post->request->get('name');
        } else {
            $name = 'Not submitted yet';
        }
        $phrase = $this
            ->getDoctrine()
            ->getRepository(Phrase::class)
            ->getPhraseById($id);
        return $this->render('@App/phrase.html.twig', ['phrase' => $phrase[0]]);
    }

    /**
     * @Route("/translate_phrase", name="translate_phrase")
     */
    public function translateAction(Request $request, EntityManagerInterface $em)
    {
        /**@var Phrase $phrase */
        $phrase = $this
            ->getDoctrine()
            ->getRepository(Phrase::class)
            ->getPhraseById($request->request->get('phrase_id'));
        foreach ($request->request as $key => $field){
            if ($key != $request->request->get('phrase_id') && $field != '' && $key != 'phrase_id'){
                $phrase[0]->translate(str_replace('_lang', '', $key))->setPhrase($field);
            }
            $em->persist($phrase[0]);
            $phrase[0]->mergeNewTranslations();
            $em->flush();
        }
        return $this->redirectToRoute('homepage');
    }
}
